package geometry;

public class Circle implements Shape
{
    private double radius;

    public Circle(double rad)
    {
        this.radius = rad;
    }

    public double getRadius()
    {
        return this.radius;
    }

    // Changing name tells us that we have to implement getArea()sss
    public double getArea()
    {
        return Math.pow(this.radius, 2)*Math.PI;
    }

    public double getPerimeter()
    {
        return this.radius*Math.PI*2;
    }
}