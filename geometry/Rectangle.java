package geometry;

public class Rectangle implements Shape
{
    private double length;
    private double width;

    public Rectangle(double lt, double wt)
    {
        this.length = lt;
        this.width = wt;
    }

    public double getLength()
    {
        return this.length;
    }

    public double getLWidth()
    {
        return this.width;
    }

    public double getArea()
    {
        return this.length*this.width;
    }

    public double getPerimeter()
    {
        return 2*this.length + 2*this.width;
    }
}
