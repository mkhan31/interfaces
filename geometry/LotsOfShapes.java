package geometry;

public class LotsOfShapes
{
    public static void main(String[] args)
    {
        Shape[] shapes = new Shape[5];
        // shapes[0] = new Shape();
        // Can't make an object from interface
        shapes[0] = new Rectangle(10, 2);
        shapes[1] = new Rectangle(5, 3);
        shapes[2] = new Circle(5);
        shapes[3] = new Circle(10);
        shapes[4] = new Square(3);
        for(Shape s : shapes)
        {
            System.out.println("Area: " + s.getArea() + "\nPerimeter: " + s.getPerimeter() + "\n");
        }
    }
}
